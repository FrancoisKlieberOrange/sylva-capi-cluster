{{- define "base-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    taints: {{ .Values.control_plane.kubeadm.nodeRegistration.taints | toYaml | nindent 6 }}
    kubeletExtraArgs: {{ mergeOverwrite (deepCopy .Values.kubelet_extra_args) (deepCopy .Values.control_plane.kubelet_extra_args) | include "quote-dict-values" |  nindent 6 }}
joinConfiguration:
  nodeRegistration:
    taints: {{ .Values.control_plane.kubeadm.nodeRegistration.taints | toYaml | nindent 6 }}
    kubeletExtraArgs: {{ mergeOverwrite (deepCopy .Values.kubelet_extra_args) (deepCopy .Values.control_plane.kubelet_extra_args) | include "quote-dict-values" | nindent 6 }}
  {{- if .Values.etcd }}
clusterConfiguration:
  etcd:
    local:
      extraArgs:
      {{- range $key, $value := .Values.etcd }}
        {{ $key }}: {{ $value | quote }}
      {{ end -}}
  {{- else }}
clusterConfiguration: {}
  {{- end }}
ntp: {{ .Values.ntp | toYaml | nindent 2 }}
preKubeadmCommands:
  {{- if (include "k8s-version-match" (tuple ">=1.29.0" .Values.k8s_version)) }}
  - |
     if [ -f "/run/kubeadm/kubeadm.yaml" ]; then       
       # kubeadm.yaml is present only on 1st node as it is used during kubeadm init process which requires super-admin.conf permissions while others use kubeadm-join-config.yaml file which requires admin.conf permissions
       ln -s /etc/kubernetes/super-admin.conf /etc/kubernetes/kube-vip.admin.conf
     else
       ln -s /etc/kubernetes/admin.conf /etc/kubernetes/kube-vip.admin.conf
     fi
  {{- end }}
  - | {{- include "kubeadm-alias-commands" (tuple "cp") | nindent 2 }}
  - echo "Preparing Kubeadm bootstrap" > /var/log/my-custom-file.log
  - | {{ include "kernel-inotify-limits" . | nindent 4 }}
  - | {{ include "containerd-config.toml-registry-config" . | nindent 4 }}
files:
{{ $kubeadmcpfiles := list }}
{{- if or (eq .Values.capi_providers.infra_provider "capo") (eq .Values.capi_providers.infra_provider "capv") (eq .Values.capi_providers.infra_provider "capm3") }}
    {{- $kubeadmcpfiles = include "kubernetes_kubeadm_vip" . | append $kubeadmcpfiles }}
{{- end }}
{{- if (.Values.registry_mirrors | dig "hosts_config" "") }}
    {{- $kubeadmcpfiles = include "registry_mirrors" . | append $kubeadmcpfiles  -}}
{{- end }}
{{- if .Values.proxies.http_proxy }}
    {{-  $kubeadmcpfiles = include "containerd_proxy_conf" . | append $kubeadmcpfiles -}}
{{- end }}
{{- $additional_files := mergeOverwrite (deepCopy .Values.additional_files) (deepCopy .Values.control_plane.additional_files) }}
{{- if $additional_files }}
    {{- $kubeadmcpfiles = tuple . $additional_files | include "additional_files" | append $kubeadmcpfiles -}}
{{- end }}
{{- if $kubeadmcpfiles -}}
  {{- range $kubeadmcpfiles -}}
    {{ . | indent 2 }}
  {{- end }}
{{- else }}
    []
{{- end }}
postKubeadmCommands:
  - set -e
  {{- if .Values.enable_longhorn }}
  - | {{ tuple .Values.capi_providers.infra_provider "cabpk" "cp" | include "shell-longhorn-node-metadata" | nindent 4 }}
  {{- end }}
{{- end }}
